package com.stolbunov.roman.domain.repository;

import com.stolbunov.roman.domain.entity.Note;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.Single;

public interface INoteRepositoryBoundary {
    Single<Note> add(Note note);

    Completable remove(Note note);

    Single<Note> update(Note note);

    Single<List<Note>> uploadNoteList();
}
