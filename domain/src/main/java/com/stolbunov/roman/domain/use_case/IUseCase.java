package com.stolbunov.roman.domain.use_case;

import com.stolbunov.roman.domain.entity.Note;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Single;

public interface IUseCase {
    Single<Note> add(Note note);

    Completable remove(Note note);

    Single<Note> update(Note note);

    Single<List<Note>> getNoteList();
}
