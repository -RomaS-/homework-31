package com.stolbunov.roman.data.repository.local_db.database_helper;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.Set;

import javax.inject.Inject;

public class DatabaseHelper extends SQLiteOpenHelper {
    private static final String DB_NAME = "notes.sql3";
    private static final int DB_VERSION = 1;

    private Set<IMigration> migrations;


    @Inject
    public DatabaseHelper(Context context, Set<IMigration> migrations) {
        super(context, DB_NAME, null, DB_VERSION);
        this.migrations = migrations;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        for (IMigration migration : migrations) {
            migration.execute(db);
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        for (IMigration migration : migrations) {
            if (migration.getTargetVersion() > oldVersion
                    && migration.getTargetVersion() <= newVersion) {
                migration.execute(db);
            }
        }
    }
}
