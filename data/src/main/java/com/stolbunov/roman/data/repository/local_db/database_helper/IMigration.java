package com.stolbunov.roman.data.repository.local_db.database_helper;

import android.database.sqlite.SQLiteDatabase;

public interface IMigration {
    int getTargetVersion();

    void execute(SQLiteDatabase database);
}
