package com.stolbunov.roman.data.repository.local_db;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.stolbunov.roman.data.repository.IDBNote;
import com.stolbunov.roman.data.repository.local_db.database_helper.DatabaseHelper;
import com.stolbunov.roman.data.repository.local_db.database_helper.entity.NoteEntity;

import java.util.LinkedList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.Completable;
import io.reactivex.Single;

import static com.stolbunov.roman.data.repository.local_db.database_helper.entity.NoteEntity.COLUMN_DESCRIPTION;
import static com.stolbunov.roman.data.repository.local_db.database_helper.entity.NoteEntity.COLUMN_ID;
import static com.stolbunov.roman.data.repository.local_db.database_helper.entity.NoteEntity.COLUMN_PRIORITY;
import static com.stolbunov.roman.data.repository.local_db.database_helper.entity.NoteEntity.COLUMN_TITLE;
import static com.stolbunov.roman.data.repository.local_db.database_helper.entity.NoteEntity.TABLE_NOTES;

public class DBNoteRepository implements IDBNote {
    private DatabaseHelper databaseHelper;

    @Inject
    DBNoteRepository(DatabaseHelper databaseHelper) {
        this.databaseHelper = databaseHelper;
    }

    @Override
    public Single<NoteEntity> add(NoteEntity note) {
        return Single.fromCallable(() -> addNewNote(note));
    }

    @Override
    public Single<NoteEntity> update(NoteEntity note) {
        return Single.fromCallable(() -> updateNote(note));
    }

    @Override
    public Single<List<NoteEntity>> getAllNotes() {
        return Single.fromCallable(this::getListNotes);
    }

    @Override
    public Completable remove(NoteEntity noteEntity) {
        return Completable.fromAction(() -> removeNote(noteEntity));
    }

    private NoteEntity addNewNote(NoteEntity noteEntity) {
        SQLiteDatabase db = databaseHelper.getWritableDatabase();
        long noteId = db.insert(TABLE_NOTES, null, noteEntity.getContentValues());
        noteEntity.setId(noteId);
        db.close();
        return noteEntity;
    }

    private NoteEntity updateNote(NoteEntity noteEntity) {
        String whereClause = COLUMN_ID + " = ?";
        String[] whereArgs = new String[]{String.valueOf(noteEntity.getId())};
        SQLiteDatabase db = databaseHelper.getWritableDatabase();

        db.update(TABLE_NOTES, noteEntity.getContentValues(), whereClause, whereArgs);
        db.close();
        return noteEntity;
    }

    private List<NoteEntity> getListNotes() {
        String query = "SELECT * FROM " + TABLE_NOTES;

        List<NoteEntity> notes = new LinkedList<>();
        SQLiteDatabase db = databaseHelper.getReadableDatabase();

        try (Cursor cursor = db.rawQuery(query, null)) {
            if (cursor.moveToFirst()) {
                do {
                    long id = cursor.getLong(cursor.getColumnIndex(COLUMN_ID));
                    String title = cursor.getString(cursor.getColumnIndex(COLUMN_TITLE));
                    String description = cursor.getString(cursor.getColumnIndex(COLUMN_DESCRIPTION));
                    int priority = cursor.getInt(cursor.getColumnIndex(COLUMN_PRIORITY));

                    notes.add(new NoteEntity(id, title, description, priority));
                } while (cursor.moveToNext());
            }
        }
        db.close();
        return notes;
    }

    private void removeNote(NoteEntity noteEntity) {
        String whereClause = COLUMN_ID + " = ?";
        String[] whereArgs = new String[]{String.valueOf(noteEntity.getId())};

        SQLiteDatabase db = databaseHelper.getWritableDatabase();
        db.delete(TABLE_NOTES, whereClause, whereArgs);
        db.close();
    }
}
