package com.stolbunov.roman.data.mapper;

import com.stolbunov.roman.data.repository.local_db.database_helper.entity.NoteEntity;
import com.stolbunov.roman.domain.entity.Note;

import java.util.ArrayList;
import java.util.List;

public class NoteMapper {
    public static NoteEntity transform(Note note) {
        return new NoteEntity(
                note.getId(),
                note.getTitle(),
                note.getDescription(),
                note.getPriorityToInt());
    }

    public static Note transform(NoteEntity noteEntity) {
        return new Note(
                noteEntity.getId(),
                noteEntity.getTitle(),
                noteEntity.getDescription(),
                noteEntity.getPriority());
    }

    public static List<Note> transformList(List<NoteEntity> notes) {
        List<Note> list = new ArrayList<>(notes.size());
        for (NoteEntity noteEntity : notes) {
            list.add(transform(noteEntity));
        }
        return list;
    }
}
