package com.stolbunov.roman.data.repository.local_db.database_helper.migration;

import android.database.sqlite.SQLiteDatabase;

import com.stolbunov.roman.data.repository.local_db.database_helper.IMigration;

import javax.inject.Inject;

public class CreateNotesTableMigration implements IMigration {

    @Inject
    public CreateNotesTableMigration() {
    }

    @Override
    public int getTargetVersion() {
        return 1;
    }

    @Override
    public void execute(SQLiteDatabase database) {
        database.execSQL("CREATE TABLE notes (" +
                "id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                "title TEXT, " +
                "description TEXT, " +
                "priority INTEGER)");
    }
}
