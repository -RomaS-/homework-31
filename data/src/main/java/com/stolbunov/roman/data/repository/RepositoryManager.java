package com.stolbunov.roman.data.repository;

import com.stolbunov.roman.data.mapper.NoteMapper;
import com.stolbunov.roman.data.repository.local_db.DBNoteRepository;
import com.stolbunov.roman.domain.entity.Note;
import com.stolbunov.roman.domain.repository.INoteRepositoryBoundary;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Completable;
import io.reactivex.Single;
import io.reactivex.schedulers.Schedulers;

public class RepositoryManager implements INoteRepositoryBoundary {
    private IDBNote db;

    @Inject
    RepositoryManager(DBNoteRepository db) {
        this.db = db;
    }

    @Override
    public Single<Note> add(Note note) {
        return Single.just(note)
                .map(NoteMapper::transform)
                .flatMap(db::add)
                .map(NoteMapper::transform)
                .subscribeOn(Schedulers.io());
    }

    @Override
    public Completable remove(Note note) {
        return db.remove(NoteMapper.transform(note))
                .subscribeOn(Schedulers.io());
    }

    @Override
    public Single<Note> update(Note note) {
        return Single.just(note)
                .map(NoteMapper::transform)
                .flatMap(db::update)
                .map(NoteMapper::transform)
                .subscribeOn(Schedulers.io());
    }

    @Override
    public Single<List<Note>> uploadNoteList() {
        return db.getAllNotes()
                .map(NoteMapper::transformList)
                .subscribeOn(Schedulers.io());
    }
}
