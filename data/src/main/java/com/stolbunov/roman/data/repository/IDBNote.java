package com.stolbunov.roman.data.repository;

import com.stolbunov.roman.data.repository.local_db.database_helper.entity.NoteEntity;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Single;

public interface IDBNote {

    Single<NoteEntity> add(NoteEntity note);

    Completable remove(NoteEntity note);

    Single<NoteEntity> update(NoteEntity note);

    Single<List<NoteEntity>> getAllNotes();
}
