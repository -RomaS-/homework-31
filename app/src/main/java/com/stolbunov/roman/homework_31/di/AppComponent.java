package com.stolbunov.roman.homework_31.di;

import android.content.Context;

import com.stolbunov.roman.homework_31.App;
import com.stolbunov.roman.homework_31.di.scope.AppScope;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjector;
import dagger.android.support.DaggerApplication;

@AppScope
@Component(modules = {DaggerAndroidModule.class, DataModule.class, MigrationDatabaseModule.class})
public interface AppComponent extends AndroidInjector<App> {

    @Component.Builder
    interface Builder {

        @BindsInstance
        Builder context(Context context);

        AndroidInjector<? extends DaggerApplication> build();
    }
}
