package com.stolbunov.roman.homework_31.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.bottomappbar.BottomAppBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.stolbunov.roman.domain.entity.Note;
import com.stolbunov.roman.homework_31.R;
import com.stolbunov.roman.homework_31.mvp.presenter.NoteListPresenter;
import com.stolbunov.roman.homework_31.mvp.view.INoteListView;
import com.stolbunov.roman.homework_31.ui.adapter.NoteAdapter;
import com.stolbunov.roman.homework_31.ui.adapter.OnItemClickListener;
import com.stolbunov.roman.homework_31.ui.fragment.AddNewNoteDialogFragment;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import dagger.android.support.DaggerAppCompatActivity;

public class MainActivity extends DaggerAppCompatActivity implements INoteListView,
        AddNewNoteDialogFragment.OnSaveDataClickListener, OnItemClickListener,
        NoteAdapter.OnRemoveNoteListener {
    private final static int RC_NOTE_EDIT = 351;
    private final static String KEY_INTENT_CHANGED_NOTE = "CHANGED_NOTE";

    @BindView(R.id.bottom_app_bar)
    BottomAppBar bottomAppBar;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.rv_notes)
    RecyclerView recyclerView;

    @Inject
    NoteListPresenter presenter;
    @Inject
    NoteAdapter adapter;

    public static Intent getIntent(Context context, Note note) {
        Intent intent = new Intent(context, MainActivity.class);
        intent.putExtra(KEY_INTENT_CHANGED_NOTE, note);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);
        setSupportActionBar(bottomAppBar);

        presenter.setNoteListView(this);
        presenter.loadDataFromDB();

        initNoteAdapter();
        initNoteRecyclerView(getManager());
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.setNoteListView(null);
    }

    private void initNoteAdapter() {
        adapter.setItemClickListener(this);
        adapter.setRemoveListener(this);
    }

    private void initNoteRecyclerView(RecyclerView.LayoutManager manager) {
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(manager);
    }

    @NonNull
    private LinearLayoutManager getManager() {
        return new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
    }

    @Override
    public void onSaveClick(Note note) {
        presenter.add(note);
    }

    @Override
    public void add(Note note) {
        adapter.add(note);
    }

    @Override
    public void remove(Note note) {
        adapter.remove(note);
    }

    @Override
    public void change(Note note) {
        adapter.change(note);
    }

    @Override
    public void setDataAdapter(List<Note> notes) {
        adapter.setData(notes);
    }

    @Override
    public void showDialog() {
        AddNewNoteDialogFragment dialogFragment = AddNewNoteDialogFragment.getInstance();
        dialogFragment.show(getSupportFragmentManager(), "AddNewNoteDialogFragment");
    }

    @Override
    public void hideDialog() {
    }

    @Override
    public void showErrorSaveNoteMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onItemClick(Note note) {
        presenter.editNote(note);
    }

    @Override
    public void onRemoveNote(Note note) {
        presenter.remove(note);
    }

    @Override
    public void goToEditor(Note note) {
        Intent intent = NoteEditorActivity.getIntent(this, note);
        startActivityForResult(intent, RC_NOTE_EDIT);
    }

    @Override
    public void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
        recyclerView.setVisibility(View.GONE);
    }

    @Override
    public void hideProgress() {
        progressBar.setVisibility(View.GONE);
        recyclerView.setVisibility(View.VISIBLE);
    }

    @Override
    public void onErrorSaveClick(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    @OnClick(R.id.fab)
    protected void onClick(View v) {
        presenter.showDialog();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case RC_NOTE_EDIT:
                    Note note = data.getParcelableExtra(KEY_INTENT_CHANGED_NOTE);
                    presenter.change(note);
                    break;
            }
        }
    }
}
