package com.stolbunov.roman.homework_31.mvp.presenter;

import com.stolbunov.roman.domain.entity.Note;
import com.stolbunov.roman.homework_31.mvp.view.INoteEditorView;

import javax.inject.Inject;


public class NoteEditorPresenter {
    private INoteEditorView view;

    @Inject
    public NoteEditorPresenter() {
    }

    public void setView(INoteEditorView view) {
        this.view = view;
    }

    public void showSelectedPriority(int priority) {
        view.showPriority(priority);
    }

    public void editNote(Note note) {
        view.editNote(note);
    }
}
