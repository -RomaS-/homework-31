package com.stolbunov.roman.homework_31.ui.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatSpinner;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import com.stolbunov.roman.domain.entity.Note;
import com.stolbunov.roman.homework_31.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;


public class AddNewNoteDialogFragment extends DialogFragment {
    private static final String KEY_NOTE = "KEY_NOTE";
    @BindView(R.id.fd_new_note_title)
    AppCompatEditText title;
    @BindView(R.id.fd_new_note_description)
    AppCompatEditText description;
    @BindView(R.id.fd_new_note_select_priority)
    AppCompatSpinner selectPriority;
    private OnSaveDataClickListener listener;
    private Unbinder unbinder;


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        listener = (OnSaveDataClickListener) context;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof OnSaveDataClickListener) {
            listener = (OnSaveDataClickListener) activity;
        } else {
            throw new IllegalStateException("Implement OnSaveDataClickListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    public static AddNewNoteDialogFragment getInstance() {
        return new AddNewNoteDialogFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_dialog_add_new_note, container, false);
        unbinder = ButterKnife.bind(this, view);
        setTransparentBackground();
        return view;
    }

    private void setTransparentBackground() {
        if (getDialog() != null && getDialog().getWindow() != null) {
            getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        }
    }

    @OnClick({R.id.fd_new_note_cancel, R.id.fd_new_note_save})
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fd_new_note_cancel:
                dismiss();
                break;
            case R.id.fd_new_note_save:
                if (checkInputData()) {
                    listener.onSaveClick(createNewNote());
                    dismiss();
                }
                break;
        }
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
    }

    private Note createNewNote() {
        return new Note(
                title.getText().toString(),
                description.getText().toString(),
                Note.NotePriority.valueOf(selectPriority.getSelectedItem().toString()));
    }

    private boolean checkInputData() {
        if (listener == null) {
            return false;
        }

        if (TextUtils.isEmpty(title.getText())) {
            listener.onErrorSaveClick(getString(R.string.fd_hint_text_title));
            return false;
        }

        if (TextUtils.isEmpty(description.getText())) {
            listener.onErrorSaveClick(getString(R.string.fd_hint_text_description));
            return false;
        }
        return true;
    }

    public interface OnSaveDataClickListener {
        void onSaveClick(Note note);

        void onErrorSaveClick(String message);
    }
}
