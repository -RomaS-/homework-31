package com.stolbunov.roman.homework_31.di;

import com.stolbunov.roman.data.repository.local_db.database_helper.IMigration;
import com.stolbunov.roman.data.repository.local_db.database_helper.migration.CreateNotesTableMigration;

import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoSet;

@Module
public interface MigrationDatabaseModule {

    @IntoSet
    @Binds
    IMigration provideCreateNotesMigration(CreateNotesTableMigration createNotesTableMigration);

}
