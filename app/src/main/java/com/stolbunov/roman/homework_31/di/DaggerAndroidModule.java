package com.stolbunov.roman.homework_31.di;

import com.stolbunov.roman.homework_31.ui.activity.MainActivity;
import com.stolbunov.roman.homework_31.ui.activity.NoteEditorActivity;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;
import dagger.android.support.AndroidSupportInjectionModule;

@Module(includes = AndroidSupportInjectionModule.class)
interface DaggerAndroidModule {

    @ContributesAndroidInjector
    MainActivity mainActivity();

    @ContributesAndroidInjector()
    NoteEditorActivity noteEditorActivity();
}
