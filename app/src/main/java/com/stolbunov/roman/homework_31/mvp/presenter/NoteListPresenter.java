package com.stolbunov.roman.homework_31.mvp.presenter;

import android.util.Log;

import com.stolbunov.roman.domain.entity.Note;
import com.stolbunov.roman.domain.use_case.IUseCase;
import com.stolbunov.roman.homework_31.mvp.view.INoteListView;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;

public class NoteListPresenter /*extends MvpPresenter<INoteListView>*/ {
    private final String ERROR_MESSAGE = "An error has occurred. Please retry the operation again.";
    private static String TAG = "TAG";
    private Disposable subscribe;

    private IUseCase useCase;
    private INoteListView view;

    @Inject
    NoteListPresenter(IUseCase useCase) {
        this.useCase = useCase;
    }

    public void setNoteListView(INoteListView view) {
        this.view = view;
    }

    public void showDialog() {
        view.showDialog();
    }

    public void hideDialog() {
        view.hideDialog();
    }

    public void loadDataFromDB() {
        view.showProgress();
        subscribe = useCase.getNoteList()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::uploadSuccessful, this::errorHandling);
    }

    public void add(Note note) {
        subscribe = useCase.add(note)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::operationSuccessful, this::errorHandling);
    }

    public void change(Note note) {
        subscribe = useCase.update(note)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(view::change, this::errorHandling);
    }

    public void remove(Note note) {
        subscribe = useCase.remove(note)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(() -> view.remove(note), this::errorHandling);
    }

    private void errorHandling(Throwable throwable) {
        Log.d(TAG, throwable.getMessage());
        view.showErrorSaveNoteMessage(ERROR_MESSAGE);
    }

    public void showErrorSaveMessage(String message) {
        view.showErrorSaveNoteMessage(message);
    }

    public void editNote(Note note) {
        view.goToEditor(note);
    }

    private void uploadSuccessful(List<Note> notes) {
        view.hideProgress();
        view.setDataAdapter(notes);
        subscribe.dispose();
    }

    private void operationSuccessful(Note note) {
        view.hideProgress();
        view.add(note);
        subscribe.dispose();
    }
}
