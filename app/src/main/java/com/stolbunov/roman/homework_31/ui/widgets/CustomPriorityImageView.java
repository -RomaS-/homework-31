package com.stolbunov.roman.homework_31.ui.widgets;

import android.content.Context;
import android.content.res.ColorStateList;
import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatImageView;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;

import com.stolbunov.roman.homework_31.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CustomPriorityImageView extends FrameLayout {
    @BindView(R.id.custom_view_center_image)
    AppCompatImageView centerImage;
    @BindView(R.id.custom_view_indicator_image)
    AppCompatImageView indicatorImage;

    public CustomPriorityImageView(@NonNull Context context) {
        this(context, null);
    }

    public CustomPriorityImageView(@NonNull Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public CustomPriorityImageView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(context);
    }

    private void initView(Context context) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.widget_priority_note, this, true);
        ButterKnife.bind(this, view);
    }

    public void setCenterImageResource(@DrawableRes int resId) {
        centerImage.setImageResource(resId);
    }

    public void setBackgroundTintList(ColorStateList tint) {
        indicatorImage.setBackgroundTintList(tint);
    }
}
