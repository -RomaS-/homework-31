package com.stolbunov.roman.homework_31.mvp.view;

import com.stolbunov.roman.domain.entity.Note;

import java.util.List;


public interface INoteListView {
    void add(Note note);

    void remove(Note note);

    void change(Note note);

    void setDataAdapter(List<Note> notes);

    void showDialog();

    void hideDialog();

    void showErrorSaveNoteMessage(String message);

    void goToEditor(Note note);

    void showProgress();

    void hideProgress();
}
