package com.stolbunov.roman.homework_31.ui.adapter;

import com.stolbunov.roman.domain.entity.Note;

public interface OnItemClickListener {
    void onItemClick(Note note);
}
