package com.stolbunov.roman.homework_31.di;

import com.stolbunov.roman.data.repository.IDBNote;
import com.stolbunov.roman.data.repository.RepositoryManager;
import com.stolbunov.roman.data.repository.local_db.DBNoteRepository;
import com.stolbunov.roman.domain.repository.INoteRepositoryBoundary;
import com.stolbunov.roman.domain.use_case.IUseCase;
import com.stolbunov.roman.domain.use_case.interactor.NoteListInteractor;
import com.stolbunov.roman.homework_31.di.scope.AppScope;

import dagger.Binds;
import dagger.Module;

@Module
public interface DataModule {

    @AppScope
    @Binds
    INoteRepositoryBoundary provideRepositoryManager(RepositoryManager repositoryManager);

    @AppScope
    @Binds
    IDBNote provideDBNoteRepository(DBNoteRepository dbNoteRepository);

    @Binds
    IUseCase provideNoteListInteractor(NoteListInteractor noteListInteractor);
}
